#!/bin/bash
# variables globales.
powered='@drowkid01'
skf=$! && uxZ=${!#@} && sxp=$?

declare -A sdata=( [slogan]='✧ | ᴅʀᴏᴡᴋɪᴅ | ✧' [soporte]='@drowkid01' )
declare -A switch=( [on]="\e[1;32m[ON]" [of]="\e[1;31m[OFF]" )
declare -A sruta=( [0]="vpscript" [ress]="vpscript/exec" [log]="vpscript/.log" )

function checkdir(){
	local dir=( "$1" "$2" "$3" "$4" )
		[[ ! -d "${dir[@]}" ]] && mkdir -p ${dir[@]}
}

function selection_fun(){
	local selection="null" range
	for((i=0; i<=$1; i++)); do range[$i]="$i "; done
	while [[ ! $(echo ${range[*]}|grep -w "$selection") ]]; do
		echo -ne "\033[1;30m <Seleccione su opción:> " >&2;read selection
		tput cuu1 >&2 && tput dl1 >&2
	done
echo $selection
}
